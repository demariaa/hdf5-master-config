

import sys
from glob import glob
from utils import getParameterListFromFilePath


if len(sys.argv) != 2:
    print '''Usage:

python %s filename

''' % sys.argv[0]
    sys.exit(0)



for arg in sys.argv[1:]:
    for filePath in glob(arg):
        try:            
	    attributeList = getParameterListFromFilePath(filePath)
            for x in sorted(attributeList):
                print x

        except Exception, e:
            print "[ERROR] %s" % ( e)


