#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#
from __future__ import print_function
import icat
import icat.config
import logging
import sys
import csv
from distutils.util import strtobool
import requests

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.CRITICAL)



def getInvestigations(conf):    
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:       
        return client.search("SELECT parameter from InvestigationParameter parameter where parameter.investigation.name='ID000000' and parameter.type.name='datasetCount' include parameter.type"  )     
    except icat.exception.ICATPrivilegesError:   
        print ("[ERROR] %s" % ( e)   )


investigations = getInvestigations(icat.config.Config().getconfig())

print(str(investigations))

       
    

