import xml.parsers.expat,sys
from glob import glob

if len(sys.argv) != 2:
    print '''Usage:

python %s filename

''' % sys.argv[0]
    sys.exit(0)


def parsefile(file):
    parser = xml.parsers.expat.ParserCreate()
    parser.ParseFile(open(file, "r"))

for arg in sys.argv[1:]:
    for filename in glob(arg):
        try:
            parsefile(filename)
            print "[PASSED] %s is well-formed" % filename
        except Exception, e:
            print "[ERROR] %s is %s" % (filename, e)
